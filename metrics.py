import lib.sewar.sewar.full_ref as f_ref
import lib.sewar.sewar.no_ref as n_ref
import numpy

# TODO fix this completely, not just ignore tests that don't work
# returns err
def full_ref(image, reference, dest={}):
    err = 0
    for metric in ["mse", "rmse", "psnr", "rmse_sw", "uqi", "ssim", "ergas", "scc", "rase", "sam", "msssim", "vifp", "psnrb"]:
        res = getattr(f_ref,metric)(image, reference)
        if isinstance(res, tuple):
            res = res[0]
        if not isinstance(res, float):
            err = f"Test {metric} skipped."
            dest[metric] = None
            continue
        res = res.__float__()
        #elif isinstance(res, (numpy.array, numpy.scalar, numpy.darray)):
        #    res = getattr(res, "tolist", lambda: res)()
        #else:
        #    res = res.item()
        #elif isinstance(res, numpy.generic):
        #    res = numpy.asscalar(res)
        #else:
        #    res = res.__float__()
        dest[metric] = res
    return err

# returns err
def no_ref(image, dest={}):
    return None

