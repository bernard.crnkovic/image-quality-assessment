import math
import numpy as np
from scipy.signal import convolve2d
import cv2
import sys
from skimage.restoration import estimate_sigma
import matplotlib.pyplot as plt
import matplotlib

# Method #1
# https://docs.scipy.org/doc/scipy/reference/generated/scipy.signal.convolve2d.html
# Noise variance https://stackoverflow.com/a/25436112
# Image kernel, convolution operator https://en.wikipedia.org/wiki/Kernel_(image_processing)
def estimate_noise_convolution(I, dest={}) -> dict:
    try:
        H, W = I.shape
        M = [[1, -2, 1],
             [-2, 4, -2],
             [1, -2, 1]]
        convolution = np.absolute(convolve2d(I, M))
        sigma = np.sum(np.sum(convolution))
        sigma = sigma * math.sqrt(0.5 * math.pi) / (6 * (W-2) * (H-2))
        dest["convolution"] = sigma.item()
    except:
        return {"desc": "Couldn't perform noise convolution test"}


# Method #2
# http://scikit-image.org/docs/dev/api/skimage.restoration.html#skimage.restoration.estimate_sigma
def estimate_noise_gaussian(image, dest={}) -> dict:
    try:
        dest["gaussian"] = estimate_sigma(image, multichannel=True, average_sigmas=True).item()
    except Exception as e:
        return {"desc": "Couldn't perform Gaussian noise estimate test."}
