import time


_TIME = time.time()

def sw(reset, data={}):
    global _TIME
    TMP = time.time() - _TIME
    _TIME = time.time() if reset else _TIME
    return TMP

def timeit(func, *args, **kwargs):
    global res
    sw(1)
    func_res = func(*args, **kwargs)
    res["durations"].append({"op": func.__name__ + '(' + ','.join(str(y) if len(str(y)) < 10 else '...' for y in args) + ',' +','.join('='.join(str(x) if len(str(x)) < 10 else '...' for x in _) for _ in kwargs) + ')', "dur": sw(0)})
    return func_res


