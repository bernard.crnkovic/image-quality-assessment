import cv2
import numpy as np
import io
import sys
import base64
from matplotlib import pyplot as plt

# src: https://opencv-python-tutroals.readthedocs.io/en/latest/py_tutorials/py_imgproc/py_histograms/py_histogram_equalization/py_histogram_equalization.html
def extract(image, dest={}):
    dest["histogram_type"] = "gray"
    hist, bins = np.histogram(image.flatten(),256,[0,256])
    return hist

def extract_rgb(r, g, b, dest={}):
    dest["histogram_type"] = "rgb"
    return extract(r), extract(g), extract(b)

def hist_points(data, dest={}):
    dest["hist_points"] = data.tolist()

def hist_points_rgb(r, g, b, dest={}):
    dest["hist_points"] = {}
    dest["hist_points"]["r"] = r.tolist()
    dest["hist_points"]["g"] = g.tolist()
    dest["hist_points"]["b"] = b.tolist()


def plot_hist_rgb(r_dat, g_dat, b_dat, dest={}, sep=False):
    plot_hist(r_dat, csumcolor='red', sep=sep, histcolor='red', alpha=0.3, legend=('R cumulative distribution function', 'Histogram'))
    plot_hist(g_dat, csumcolor='green', sep=sep, histcolor='green', alpha=0.3, legend=('G cumulative distribution function', 'Histogram'))
    plot_hist(r_dat, csumcolor='blue', sep=sep, histcolor='blue', alpha=0.3, legend=('B cumulative distribution function', 'Histogram'))
    save_plot(dest=dest)

    #plt.legend(('R cumulative sum', 'G cumulative sum', 'B cumulative sum', 'R histogram', 'G histogram', 'B histogram'), loc='upper left')

def plot_hist(data, csumcolor='purple', sep=False, histcolor='gray', alpha=1,format="svg", dest={}, legend=('Cumulative distribution function', 'Histogram')):
    if sep:
        plt.subplots(1,3)
    cdf = data.cumsum()
    cdf_normalized = cdf * data.max() / cdf.max()
    plt.margins(0,0)
    plt.plot(cdf_normalized, color=csumcolor)
    plt.hist(data.flatten(), 256, [0, 256], color=histcolor, alpha=alpha)
    plt.xlim([0, 256])
    plt.legend(legend, loc='upper left')
    return plt

def clear_plot():
    plt.cla()
    plt.clf()

def save_plot(format="svg", dest={}):
    if format == "svg":
        buf = io.StringIO()
        plt.savefig(buf, format="svg")
        plt.savefig("fig.png", format="png")
        buf.seek(0)
        dest["hist"] = buf.getvalue()
        return
    buf = io.BytesIO()
    plt.savefig(buf, format=format)
    dest["hist"] = base64.b64encode(buf.getvalue()).decode()
    clear_plot()
