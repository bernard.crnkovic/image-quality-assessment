#! ./venv/bin/ python

# libs
from __future__ import print_function
import json
import yaml
import os
import argparse
import sys
import glob
import mimetypes
import cv2
import time

# scripts
import blur
import gamma
import noise
import metrics
import errors
from stopwatch import timeit, sw

# exit code, may change if some operation wasn't successful
EXIT_CODE = 0
res = {
        "err": [],
        "images": [],
        "durations": []
      }

def return_result():
    print(json.dumps(res) if not args["yml"] else yaml.dump(res, default_flow_style=False))
    sys.exit(EXIT_CODE)

def eprint(*args, **kwargs):
    print(*args, file=sys.stderr, **kwargs)

# construct the argument parse and parse the arguments
ap = argparse.ArgumentParser()

### ARGUMENT PARSING

# GENERAL
ap.add_argument_group("General")
ap.add_argument("--debug", action="store_true", dest="dbg",
                help="include debug info in output")
ap.add_argument("--all", action="store_true", dest="all",
                help="perform all IQA operations (warning: slow operation)")
ap.add_argument("--rgb", action="store_true", dest="rgb",
                help="perform operations on all channels of image separately where such operation makes sense")
ap.add_argument("--separate", action="store_true", dest="sep",
                help="creates separate histogram for each channel")
ap.add_argument_group("Output formatting")
ap.add_argument("-y", "--yml", "--yaml", action="store_true", dest="yml",
                help="outputs in YAML instead of JSON (default)")
ap.add_argument("-o", "--output", action="store_true", dest="o",
                help="specify output directory for resources, if specified, script doesn't return anything")

# METRICS
ap.add_argument_group("Metrics")
ap.add_argument("-m", "--metrics", action='store_true', dest="metrics",
        help="perform general image quality assessment metrics with referent images: Mean Squared Error (MSE), Root Mean Sqaured Error (RMSE), Peak Signal-to-Noise Ratio (PSNR), Structural Similarity Index (SSIM), Universal Quality Image Index (UQI), Multi-scale Structural Similarity Index (MS-SSIM), Erreur Relative Globale Adimensionnelle de Synthèse (ERGAS), Spatial Correlation Coefficient (SCC), Relative Average Spectral Error (RASE), Spectral Angle Mapper (SAM), Spectral Distortion Index (D_lambda), Spatial Distortion Index (D_S), Quality with No Reference (QNR), Visual Information Fidelity (VIF), Block Sensitive - Peak Signal-to-Noise Ratio (PSNR-B) SOURCE: https://github.com/andrewekhalel/sewar")

# BLUR
ap.add_argument_group("Blur")
ap.add_argument("-b", "--blur", action="store_true", dest="blur",
                help="perform all blur assessment operations")
ap.add_argument("--bl", "--blur-laplacian", action="store_true", dest="blur_laplacian",
                help="calculate laplacian of image and sum pixel values in resulting image")
ap.add_argument("--blt", "--blur-laplacian-threshold", metavar="VALUE", type=float, default=100.0, dest="blur_laplacian_threshold",
                help="focus measures that fall below this value will be considered 'blurry'")

# GAMMA
ap.add_argument_group("Gamma")
ap.add_argument("-g", "--gamma", action="store_true", dest="gamma",
                help="perform all gamma assessment operations")
ap.add_argument("--format", metavar="FORMAT", default="svg", type=str, dest="format",
                help="generate gamma histograms in specified format (svg/png)")
ap.add_argument("-p", "--points", action="store_true", dest="points",
                help="include plot data")

# NOISE
ap.add_argument_group("Noise")
ap.add_argument("-n", "--noise", action="store_true", dest="noise",
                help="perform all noise assessment operations")
ap.add_argument("--ng", "--noise-gaussian", action="store_true", dest="noise_gaussian",
                help="calculate standard deviation assuming gaussian noise")
ap.add_argument("--ngt", "--noise-gaussian-threshold", metavar="VALUE", default=100.0, type=float, dest="noise_gaussian_threshold",
                help="focus measures that fall below this value will be considered 'blurry'")

# POSITIONALS
ap.add_argument("path_regex", type=str,
                help="GLOB path regex")
ap.add_argument("ref_path", nargs="?",type=str, default=None,
                help="reference images path, mandatory if metrics are being calculated flag is used!")

args = vars(ap.parse_args())
if args["dbg"]:
    res["args"] = args

### COLLECTING PATHS
# libmagic should be used for content inspection...
mimetypes.init()
images = list(filter(lambda x: "image" in mimetypes.guess_type(x)[0] , glob.iglob(args["path_regex"], recursive=True)))
if len(images) > 1 and args["ref_path"]:
    res["err"].append({"code": errors.ARGS_LOGIC , "desc": "No regex allowed when reference image is supplied"})
    args["ref_path"] = None
    EXIT_CODE = 1
if args["metrics"] and not args["ref_path"]:
    res["err"].append({"desc": "Reference image not supplied, some tests won't be conducted"})
    EXIT_CODE = 1

### POPULATING JSONS
for path in images:
    js = {
            "name": os.path.basename(path),
            "path": os.path.abspath(path),
         }
    original = cv2.imread(path)
    js["read_time"] = sw(1)
    blue, green, red = cv2.split(original)
    js["channeling_time"] = sw(1)
    grayscale = cv2.cvtColor( original, cv2.COLOR_BGR2GRAY)
    js["grayscaling_time"] = sw(1)
    js["dim"] =  list(original.shape)[:2]
    #reference = None
    if args["ref_path"]:
        reference = cv2.imread(args["ref_path"])
        js["ref_read_time"] = sw(1)
    # measurements
    err = []
    try:
        if args["metrics"]:
            js["metrics"] = {}
            if args["ref_path"]:
                err.append(metrics.full_ref(original, reference, dest=js["metrics"]))
            err.append(metrics.no_ref(original, dest=js["metrics"]))
            js["metrics"]["time"] = sw(1)
        if args["blur"]:
            js["blur"] = {}
            blur.variance_of_laplacian(grayscale, dest=js["blur"])
            js["blur"]["time"] = sw(1)
        if args["gamma"]:
            js["gamma"] = {}
            j = js["gamma"]
            if args["rgb"]:
                r_dat, g_dat, b_dat = gamma.extract_rgb(red, green, blue, dest=j)
                if args["points"]:
                    gamma.hist_points_rgb(r_dat, g_dat, b_dat, dest=j)
                else:
                    gamma.plot_hist_rgb(r_dat, g_dat, b_dat, sep=args["sep"], dest=j)
                    gamma.save_plot(dest=j, format=args["format"])
            else:
                dat = gamma.extract(grayscale, dest=j)
                if args["points"]:
                    gamma.hist_points(dat, dest=j)
                else:
                    gamma.plot_hist(dat, dest=j)
                    gamma.save_plot(dest=j, format=args["format"])
                #err.append(gamma.hist_img(gamma.get_hist(grayscale), format=args["gamma_hist"], dest=j))
            j["time"] = sw(1)
        if args["noise"]:
            js["noise"] = {}
            j = js["noise"]
            err.append(noise.estimate_noise_gaussian(original, dest=j))
            err.append(noise.estimate_noise_convolution(grayscale, dest=j))
            js["noise"]["time"] = sw(1)
    except Exception as e: # not printing error because script must return JSON, even incomplete!
        raise e
        EXIT_CODE = 1
    res["err"] += filter(lambda x: x, err)
    res["images"].append(js)

return_result()
