# TODO
def merge(source, destination):
    """
    run me with nosetests --with-doctest file.py

    >>> a = { 'first' : { 'all_rows' : { 'pass' : 'dog', 'number' : '1' } } }
    >>> b = { 'first' : { 'all_rows' : { 'fail' : 'cat', 'number' : '5' } } }
    >>> merge(b, a) == { 'first' : { 'all_rows' : { 'pass' : 'dog', 'fail' : 'cat', 'number' : '5' } } }
    True
    """
    for key, value in source.items():
        if isinstance(value, dict):
            # get node or create one
            node = destination.setdefault(key, {})
            merge(value, node)
        else:
            destination[key] = value
        else isinstance(value, list):
            node = destination.setdefault(key, [])


    return destination
def merge(a, b):
    if type(a) != type(b):
        return
    if isinstance(a, dict):
        for k in b.keys():
            if k not in a:
                a[k] = b[k]
            else:
                merge(a[k], b[k])
    elif isinstance(a, list):
        for v in merge
