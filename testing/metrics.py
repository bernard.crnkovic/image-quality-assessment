import sewar.full_ref

# includes metrics
# mse, rmse, psnr, rmse_sw, uqi, ssim, ergas, scc, rase, sam, msssim, vifp, psnrb
met = dict(mse=sewar.full_ref.mse,
                rmse=sewar.full_ref.rmse,
                psnr=sewar.full_ref.psnr,
                rmse_sw=sewar.full_ref.rmse_sw,
                uqi=sewar.full_ref.uqi,
                ssim=sewar.full_ref.ssim,
                ergas=sewar.full_ref.ergas,
                scc=sewar.full_ref.scc,
                rase=sewar.full_ref.rase,
                sam=sewar.full_ref.sam,
                msssim=sewar.full_ref.msssim,
                vifp=sewar.full_ref.vifp,
                psnrb=sewar.full_ref.psnrb,
                )

def get(image, *args):
    d = {}
    for v in args:
        d[v]=met[v](image)
    return d
